//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose:		Pistol - hand gun
//			Why is it shared now? Well, gross, hacky shit for the dual pistols.
//
//=============================================================================//

#include "cbase.h"
#include "npcevent.h"
#include "basehlcombatweapon_shared.h"

#ifdef HL2CE_PORTAL
#include "portal_player_shared.h"
#else
#include "hl2_player_shared.h"
#endif

#include "gamerules.h"
#include "in_buttons.h"
#include "vstdlib/random.h"
#include "gamestats.h"
#include "datacache/imdlcache.h"

#ifndef CLIENT_DLL
#include "soundent.h"
#include "game.h"
#include "basecombatcharacter.h"
#include "ai_basenpc.h"
#else
#include "c_basehlcombatweapon.h"
#include "c_weapon__stubs.h"
#endif

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

#define	PISTOL_FASTEST_REFIRE_TIME		0.1f
#define	PISTOL_FASTEST_DRY_REFIRE_TIME	0.2f

#define	PISTOL_ACCURACY_SHOT_PENALTY_TIME		0.2f	// Applied amount of time each shot adds to the time we must recover from
#define	PISTOL_ACCURACY_MAXIMUM_PENALTY_TIME	1.5f	// Maximum penalty to deal out

ConVar	pistol_use_new_accuracy( "pistol_use_new_accuracy", "1" );

#ifdef CLIENT_DLL
#define CWeaponPistol C_WeaponPistol
#endif

//-----------------------------------------------------------------------------
// CWeaponPistol
//-----------------------------------------------------------------------------
class CWeaponPistol : public CBaseHLCombatWeapon
{
	DECLARE_CLASS(CWeaponPistol, CBaseHLCombatWeapon);

	DECLARE_NETWORKCLASS();
	DECLARE_PREDICTABLE();
public:
	CWeaponPistol(void);

	void	Precache( void );
	void	ItemPostFrame( void );
	void	ItemPreFrame( void );
	void	ItemBusyFrame( void );
	void	PrimaryAttack( void );
	void	SecondaryAttack();
	void	AddViewKick( void );
	void	DryFire( void );

	void	UpdatePenaltyTime( void );

#ifndef CLIENT_DLL
	void	Operator_HandleAnimEvent( animevent_t *pEvent, CBaseCombatCharacter *pOperator );
	int		CapabilitiesGet( void ) { return bits_CAP_WEAPON_RANGE_ATTACK1; }
	DECLARE_DATADESC();
#endif

	Activity	GetPrimaryAttackActivity( void );

	virtual bool Reload( void );

	virtual const Vector& GetBulletSpread(void)
	{
		// Handle NPCs first
		static Vector npcCone = VECTOR_CONE_5DEGREES;
		if (GetOwner() && GetOwner()->IsNPC())
			return npcCone;

		static Vector cone;
		Vector Vec1;
		Vector Vec2;

		if (m_bCurrentlyDualPistols) {
			Vec1 = VECTOR_CONE_3DEGREES;
			Vec2 = VECTOR_CONE_8DEGREES;
		}
		else {
			Vec1 = VECTOR_CONE_1DEGREES;
			Vec2 = VECTOR_CONE_6DEGREES;
		}

		if (pistol_use_new_accuracy.GetBool())
		{
			float ramp = RemapValClamped(m_flAccuracyPenalty,
				0.0f,
				PISTOL_ACCURACY_MAXIMUM_PENALTY_TIME,
				0.0f,
				1.0f);

			// We lerp from very accurate to inaccurate over time
			VectorLerp(Vec1, Vec2, ramp, cone);
		}
		else
		{
			// Old value
			cone = VECTOR_CONE_4DEGREES;
		}

		return cone;
	}
	
	virtual int	GetMinBurst() 
	{ 
		return 1; 
	}

	virtual int	GetMaxBurst() 
	{ 
		return 3; 
	}

	virtual float GetFireRate( void ) 
	{
		return 0.5f; 
	}

	DECLARE_ACTTABLE();
	
	WeaponTypes GetWeaponType() { return WEAPON_TYPE_PISTOL; }
	
	const char	*GetViewModel( int viewmodelindex = 0 ) const;
	void	OnRestore( void );
	bool	Deploy(void);
	void	Spawn(void);

	bool IsDualWeapon()
	{
		if (m_bCurrentlyDualPistols)
			return true;
		else
			return false;
	}

	int GetMaxClip1() const
	{
		if ( m_bCurrentlyDualPistols )
			return (GetWpnData().iMaxClip1 * 2);
		else
			return GetWpnData().iMaxClip1;
	}

	void	BasePrimaryAttack(void);

	virtual const char		*GetWorldModel( void ) const;
	virtual int				GetWorldModelIndex( void );
	virtual void SetWeaponModelIndex(const char *pName);
private:
	void	ActivateSingleModel( void );
	void	ActivateAkimboModel( void );
	
	CNetworkVar( float,	m_flSoonestPrimaryAttack );
	CNetworkVar( float,	m_flLastAttackTime );
	CNetworkVar( float,	m_flAccuracyPenalty );
	CNetworkVar( int,	m_nNumShotsFired );
	CNetworkVar(int, m_iStoredLeftAmmo);
	CNetworkVar(int, m_iSingleViewIndex);
	CNetworkVar(int, m_iAkimboViewIndex);
	CNetworkVar(bool, m_bCurrentlyDualPistols);
	CNetworkVar(bool, m_bLeftShot);
	CNetworkVar(bool, m_bFirstTimeGoingDual);
	
	int m_silencedModelIndex;
	bool m_inPrecache;
	CWeaponPistol( const CWeaponPistol & );
};

IMPLEMENT_NETWORKCLASS_ALIASED( WeaponPistol, DT_WeaponPistol);

BEGIN_NETWORK_TABLE( CWeaponPistol, DT_WeaponPistol )
#ifndef CLIENT_DLL
SendPropTime(SENDINFO(m_flSoonestPrimaryAttack)),
SendPropTime(SENDINFO(m_flLastAttackTime)),
SendPropFloat(SENDINFO(m_flAccuracyPenalty)),
SendPropInt(SENDINFO(m_nNumShotsFired)),
SendPropInt(SENDINFO(m_iStoredLeftAmmo)),
SendPropInt(SENDINFO(m_iSingleViewIndex)),
SendPropInt(SENDINFO(m_iAkimboViewIndex)),
SendPropBool(SENDINFO(m_bCurrentlyDualPistols)),
SendPropBool(SENDINFO(m_bLeftShot)),
SendPropBool(SENDINFO(m_bFirstTimeGoingDual)),
#else
RecvPropTime(RECVINFO(m_flSoonestPrimaryAttack)),
RecvPropTime(RECVINFO(m_flLastAttackTime)),
RecvPropFloat(RECVINFO(m_flAccuracyPenalty)),
RecvPropInt(RECVINFO(m_nNumShotsFired)),
RecvPropInt(RECVINFO(m_iStoredLeftAmmo)),
RecvPropInt(RECVINFO(m_iSingleViewIndex)),
RecvPropInt(RECVINFO(m_iAkimboViewIndex)),
RecvPropBool(RECVINFO(m_bCurrentlyDualPistols)),
RecvPropBool(RECVINFO(m_bLeftShot)),
RecvPropBool(RECVINFO(m_bFirstTimeGoingDual)),
#endif
END_NETWORK_TABLE()

LINK_ENTITY_TO_CLASS( weapon_pistol, CWeaponPistol );
PRECACHE_WEAPON_REGISTER( weapon_pistol );

#ifndef CLIENT_DLL
BEGIN_DATADESC( CWeaponPistol )

	DEFINE_FIELD( m_flSoonestPrimaryAttack, FIELD_TIME ),
	DEFINE_FIELD( m_flLastAttackTime,		FIELD_TIME ),
	DEFINE_FIELD( m_flAccuracyPenalty,		FIELD_FLOAT ), //NOTENOTE: This is NOT tracking game time
	DEFINE_FIELD( m_nNumShotsFired,			FIELD_INTEGER ),
	DEFINE_FIELD( m_bCurrentlyDualPistols,	FIELD_BOOLEAN ),
	DEFINE_FIELD( m_bLeftShot,				FIELD_BOOLEAN ),
	DEFINE_FIELD( m_iStoredLeftAmmo,		FIELD_INTEGER ),
	DEFINE_FIELD( m_bFirstTimeGoingDual,	FIELD_BOOLEAN ),

END_DATADESC()
#endif

#ifdef CLIENT_DLL
BEGIN_PREDICTION_DATA( CWeaponPistol )
	DEFINE_PRED_FIELD( m_flSoonestPrimaryAttack, FIELD_FLOAT, FTYPEDESC_INSENDTABLE ),
	DEFINE_PRED_FIELD( m_flLastAttackTime, FIELD_FLOAT, FTYPEDESC_INSENDTABLE ),
	DEFINE_PRED_FIELD( m_flAccuracyPenalty, FIELD_FLOAT, FTYPEDESC_INSENDTABLE ),
	DEFINE_PRED_FIELD( m_nNumShotsFired, FIELD_INTEGER, FTYPEDESC_INSENDTABLE ),
END_PREDICTION_DATA()
#endif

acttable_t	CWeaponPistol::m_acttable[] = 
{
	{ ACT_IDLE,						ACT_IDLE_PISTOL,				true },
	{ ACT_IDLE_ANGRY,				ACT_IDLE_ANGRY_PISTOL,			true },
	{ ACT_RANGE_ATTACK1,			ACT_RANGE_ATTACK_PISTOL,		true },
	{ ACT_RELOAD,					ACT_RELOAD_PISTOL,				true },
	{ ACT_WALK_AIM,					ACT_WALK_AIM_PISTOL,			true },
	{ ACT_RUN_AIM,					ACT_RUN_AIM_PISTOL,				true },
	{ ACT_GESTURE_RANGE_ATTACK1,	ACT_GESTURE_RANGE_ATTACK_PISTOL,true },
	{ ACT_RELOAD_LOW,				ACT_RELOAD_PISTOL_LOW,			false },
	{ ACT_RANGE_ATTACK1_LOW,		ACT_RANGE_ATTACK_PISTOL_LOW,	false },
	{ ACT_COVER_LOW,				ACT_COVER_PISTOL_LOW,			false },
	{ ACT_RANGE_AIM_LOW,			ACT_RANGE_AIM_PISTOL_LOW,		false },
	{ ACT_GESTURE_RELOAD,			ACT_GESTURE_RELOAD_PISTOL,		false },
	{ ACT_WALK,						ACT_WALK_PISTOL,				false },
	{ ACT_RUN,						ACT_RUN_PISTOL,					false },

	{ ACT_RANGE_ATTACK1,				ACT_RANGE_ATTACK_PISTOL,				false },
	{ ACT_MP_STAND_IDLE,				ACT_DOD_STAND_AIM_PISTOL,				false },
	{ ACT_MP_CROUCH_IDLE,				ACT_DOD_CROUCH_IDLE_PISTOL,				false },
	{ ACT_MP_RUN,						ACT_DOD_WALK_IDLE_PISTOL,				false },
	{ ACT_MP_CROUCHWALK,				ACT_DOD_CROUCHWALK_IDLE_PISTOL,			false },
	{ ACT_MP_ATTACK_STAND_PRIMARYFIRE,	ACT_DOD_PRIMARYATTACK_PISTOL,			false },
	{ ACT_MP_ATTACK_CROUCH_PRIMARYFIRE,	ACT_DOD_PRIMARYATTACK_PISTOL,			false },
	{ ACT_MP_RELOAD_STAND,				ACT_DOD_RELOAD_PISTOL,					false },
	{ ACT_MP_RELOAD_CROUCH,				ACT_DOD_RELOAD_CROUCH_PISTOL,			false },
	{ ACT_MP_JUMP,						ACT_MP_JUMP,							false },
};


IMPLEMENT_ACTTABLE( CWeaponPistol );

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CWeaponPistol::CWeaponPistol( void )
{
	m_flSoonestPrimaryAttack = gpGlobals->curtime;
	m_flAccuracyPenalty = 0.0f;

	m_fMinRange1		= 24;
	m_fMaxRange1		= 1500;
	m_fMinRange2		= 24;
	m_fMaxRange2		= 200;

	m_bFiresUnderwater	= true;
	m_bAltFiresUnderwater = true;

	m_bCurrentlyDualPistols = false;
	m_bLeftShot = false;

	m_iStoredLeftAmmo = GetWpnData().iMaxClip1;
	m_bFirstTimeGoingDual = true;
	m_inPrecache = false;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWeaponPistol::Precache( void )
{
	m_inPrecache = true;

	PrecacheModel("models/weapons/v_pistol.mdl");
	PrecacheModel("models/weapons/v_pistol_2.mdl");
	PrecacheModel("models/weapons/w_pistol.mdl");
	PrecacheModel("models/weapons/w_pistl2_TWO.mdl");

	m_iSingleViewIndex		= GetWpnData().szViewModel;
	m_iAkimboViewIndex		= GetWpnData().szViewModelAkimbo;

	m_silencedModelIndex = CBaseEntity::PrecacheModel( GetWpnData().szWorldModelAkimbo );

	BaseClass::Precache();
	m_inPrecache = false;
}

#ifndef CLIENT_DLL
//-----------------------------------------------------------------------------
// Purpose:
// Input  :
// Output :
//-----------------------------------------------------------------------------
void CWeaponPistol::Operator_HandleAnimEvent( animevent_t *pEvent, CBaseCombatCharacter *pOperator )
{
	switch( pEvent->event )
	{
		case EVENT_WEAPON_PISTOL_FIRE:
		{
			Vector vecShootOrigin, vecShootDir;
			vecShootOrigin = pOperator->Weapon_ShootPosition();

			CAI_BaseNPC *npc = pOperator->MyNPCPointer();
			ASSERT( npc != NULL );

			vecShootDir = npc->GetActualShootTrajectory( vecShootOrigin );

			CSoundEnt::InsertSound( SOUND_COMBAT|SOUND_CONTEXT_GUNFIRE, pOperator->GetAbsOrigin(), SOUNDENT_VOLUME_PISTOL, 0.2, pOperator, SOUNDENT_CHANNEL_WEAPON, pOperator->GetEnemy() );

			WeaponSound( SINGLE_NPC );
			pOperator->FireBullets( 1, vecShootOrigin, vecShootDir, VECTOR_CONE_PRECALCULATED, MAX_TRACE_LENGTH, m_iPrimaryAmmoType, 2 );
			pOperator->DoMuzzleFlash();
			m_iClip1 = m_iClip1 - 1;
		}
		break;
		default:
			BaseClass::Operator_HandleAnimEvent( pEvent, pOperator );
			break;
	}
}
#endif

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
void CWeaponPistol::DryFire( void )
{
	WeaponSound( EMPTY );
	SendWeaponAnim( ACT_VM_DRYFIRE );
	
	m_flSoonestPrimaryAttack	= gpGlobals->curtime + PISTOL_FASTEST_DRY_REFIRE_TIME;
	m_flNextPrimaryAttack		= gpGlobals->curtime + SequenceDuration();
}

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
void CWeaponPistol::PrimaryAttack( void )
{
	if ( ( gpGlobals->curtime - m_flLastAttackTime ) > 0.5f )
	{
		m_nNumShotsFired = 0;
	}
	else
	{
		m_nNumShotsFired++;
	}

	m_flLastAttackTime = gpGlobals->curtime;
	m_flSoonestPrimaryAttack = gpGlobals->curtime + PISTOL_FASTEST_REFIRE_TIME;

#ifndef CLIENT_DLL
	CSoundEnt::InsertSound( SOUND_COMBAT, GetAbsOrigin(), SOUNDENT_VOLUME_PISTOL, 0.2, GetOwner() );
#endif

	CBasePlayer *pOwner = ToBasePlayer( GetOwner() );

	if( pOwner )
	{
		// Each time the player fires the pistol, reset the view punch. This prevents
		// the aim from 'drifting off' when the player fires very quickly. This may
		// not be the ideal way to achieve this, but it's cheap and it works, which is
		// great for a feature we're evaluating. (sjb)
		pOwner->ViewPunchReset();
	}

	BasePrimaryAttack();

	// Add an accuracy penalty which can move past our maximum penalty time if we're really spastic
	m_flAccuracyPenalty += PISTOL_ACCURACY_SHOT_PENALTY_TIME;

	m_iPrimaryAttacks++;
#ifndef CLIENT_DLL
	gamestats->Event_WeaponFired( pOwner, true, GetClassname() );
#endif
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWeaponPistol::UpdatePenaltyTime( void )
{
	CBasePlayer *pOwner = ToBasePlayer( GetOwner() );

	if ( pOwner == NULL )
		return;

	// Check our penalty time decay
	if ( ( ( pOwner->m_nButtons & IN_ATTACK ) == false ) && ( m_flSoonestPrimaryAttack < gpGlobals->curtime ) )
	{
		m_flAccuracyPenalty -= gpGlobals->frametime;
		m_flAccuracyPenalty = clamp( m_flAccuracyPenalty, 0.0f, PISTOL_ACCURACY_MAXIMUM_PENALTY_TIME );
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWeaponPistol::ItemPreFrame( void )
{
	UpdatePenaltyTime();

	BaseClass::ItemPreFrame();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWeaponPistol::ItemBusyFrame( void )
{
	UpdatePenaltyTime();

	BaseClass::ItemBusyFrame();
}

//-----------------------------------------------------------------------------
// Purpose: Allows firing as fast as button is pressed
//-----------------------------------------------------------------------------
void CWeaponPistol::ItemPostFrame( void )
{
	BaseClass::ItemPostFrame();

	if ( m_bInReload )
		return;
	
	CBasePlayer *pOwner = ToBasePlayer( GetOwner() );

	if ( pOwner == NULL )
		return;

	//Allow a refire as fast as the player can click
	if ( ( ( pOwner->m_nButtons & IN_ATTACK ) == false ) && ( m_flSoonestPrimaryAttack < gpGlobals->curtime ) )
	{
		m_flNextPrimaryAttack = gpGlobals->curtime - 0.1f;
	}
	else if ( ( pOwner->m_nButtons & IN_ATTACK ) && ( m_flNextPrimaryAttack < gpGlobals->curtime ) && ( m_iClip1 <= 0 ) )
	{
		DryFire();
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
// Output : int
//-----------------------------------------------------------------------------
Activity CWeaponPistol::GetPrimaryAttackActivity( void )
{
	if (m_bCurrentlyDualPistols)
	{

		if (m_bLeftShot){
			m_bLeftShot = false;
			return ACT_VM_PRIMARYATTACK;
		}
		else{
			m_bLeftShot = true;
			return ACT_VM_SECONDARYATTACK;
		}
	}
	else
	{
		if (m_nNumShotsFired < 1)
			return ACT_VM_PRIMARYATTACK;

		if (m_nNumShotsFired < 2)
			return ACT_VM_RECOIL1;

		if (m_nNumShotsFired < 3)
			return ACT_VM_RECOIL2;

		return ACT_VM_RECOIL3;
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
bool CWeaponPistol::Reload(void)
{
	int iClipSize1 = GetMaxClip1();

	CBaseCombatCharacter *pOwner = GetOwner();
	if (!pOwner)
		return false;

	// If I don't have any spare ammo, I can't reload
	if (pOwner->GetAmmoCount(m_iPrimaryAmmoType) <= 0)
		return false;

	bool bReload = false;

	// need to reload primary clip?
	int primary = MIN(iClipSize1 - m_iClip1, pOwner->GetAmmoCount(m_iPrimaryAmmoType));
	if (primary != 0)
	{
		bReload = true;
	}

	if (!bReload)
		return false;

	// Play reload
	WeaponSound(RELOAD);
	SendWeaponAnim(ACT_VM_RELOAD);

	// Play the player's reload animation
	if (pOwner->IsPlayer())
	{
		((CBasePlayer *)pOwner)->SetAnimation(PLAYER_RELOAD);

#ifdef HL2CE_PORTAL
		((CPortal_Player *)pOwner)->DoAnimationEvent(PLAYERANIMEVENT_RELOAD , 0);
#endif
	}

	MDLCACHE_CRITICAL_SECTION();
	float flSequenceEndTime = gpGlobals->curtime + SequenceDuration();
	pOwner->SetNextAttack(flSequenceEndTime);
	m_flNextPrimaryAttack = m_flNextSecondaryAttack = flSequenceEndTime;

	m_bInReload = true;

	return true;

}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWeaponPistol::AddViewKick( void )
{
	CBasePlayer *pPlayer  = ToBasePlayer( GetOwner() );
	
	if ( pPlayer == NULL )
		return;

	QAngle	viewPunch;

	viewPunch.x = random->RandomFloat( 0.25f, 0.5f );
	viewPunch.y = random->RandomFloat( -.6f, .6f );
	viewPunch.z = 0.0f;

	//Add it to the view punch
	pPlayer->ViewPunch( viewPunch );
}

void CWeaponPistol::Spawn()
{
	Precache();
	BaseClass::Spawn();
}

const char *CWeaponPistol::GetViewModel(int) const
{
	if (m_iViewModelIndex == m_iSingleViewIndex)
	{
		return GetWpnData().szViewModel;
	}
	else
	{
		return GetWpnData().szViewModelAkimbo;
	}
}

void CWeaponPistol::SecondaryAttack()
{
	CHL2_Player *pPlayer = ToHL2Player(GetOwner());

	if (!pPlayer)
		return;

	if (!pPlayer->m_bCanUseDualPistols)
		return;

	if (m_bCurrentlyDualPistols)
	{
		ActivateSingleModel();

		//stored ammo = half the current mag
		m_iStoredLeftAmmo = m_iClip1 / 2;
		m_iClip1 = m_iClip1 / 2;

		m_bCurrentlyDualPistols = false;
		m_bLeftShot = false;
		pPlayer->m_bWeaponIsAkimbo = false;
	}
	else
	{
		ActivateAkimboModel();

		//add the current stored ammo to the mag
		if (m_bFirstTimeGoingDual)
			m_iClip1 = m_iClip1 + GetWpnData().iMaxClip1;
		else
			m_iClip1 = m_iClip1 + m_iStoredLeftAmmo;

		m_iStoredLeftAmmo = 0;

		m_bCurrentlyDualPistols = true;
		pPlayer->m_bWeaponIsAkimbo = true;
		m_bFirstTimeGoingDual = false;
	}

	SendWeaponAnim(ACT_VM_DRAW);

	m_flNextPrimaryAttack = gpGlobals->curtime + 0.8f;
	m_flNextSecondaryAttack = gpGlobals->curtime + 0.8f;
	
	SetWeaponModelIndex( GetWorldModel() );

}

void CWeaponPistol::SetWeaponModelIndex( const char *pName )
{
 	 m_iWorldModelIndex = modelinfo->GetModelIndex( pName );
}

void CWeaponPistol::ActivateSingleModel(void)
{
	m_iViewModelIndex = m_iSingleViewIndex;
	SetModel(GetViewModel());
}

void CWeaponPistol::ActivateAkimboModel(void)
{
	m_iViewModelIndex = m_iAkimboViewIndex;
	SetModel(GetViewModel());
}

void CWeaponPistol::OnRestore( void )
{
	BaseClass::OnRestore();
	if ( m_bCurrentlyDualPistols )
	{
		ActivateAkimboModel();
	}
	else
	{
		ActivateSingleModel();
	}
}

bool CWeaponPistol::Deploy(void)
{
	SetWeaponIdleTime( gpGlobals->curtime + random->RandomFloat( 10, 15 ) );

	if ( m_bCurrentlyDualPistols )
	{
		ActivateAkimboModel();
	}
	else
	{
		ActivateSingleModel();
	}

	CHL2_Player *pPlayer = ToHL2Player(GetOwner());

	bool bRet = BaseClass::Deploy();
	if ( bRet )
	{
		// 
		if ( pPlayer )
		{
			pPlayer->SetNextAttack( gpGlobals->curtime + 1.0 );
		}
	}

	if (m_bCurrentlyDualPistols)
		pPlayer->m_bWeaponIsAkimbo = true;

	return bRet;

}

//-----------------------------------------------------------------------------
// Purpose: Primary fire button attack
//-----------------------------------------------------------------------------
void CWeaponPistol::BasePrimaryAttack(void)
{
	// If my clip is empty (and I use clips) start reload
	if ( !m_iClip2 && !m_iClip1 ) 
	{
		Reload();
		return;
	}

	// Only the player fires this way so we can cast
	CBasePlayer *pPlayer = ToBasePlayer( GetOwner() );

	if (!pPlayer)
		return;

	pPlayer->DoMuzzleFlash();

	SendWeaponAnim( GetPrimaryAttackActivity() );

	// player "shoot" animation
	pPlayer->SetAnimation( PLAYER_ATTACK1 );

#ifdef HL2CE_PORTAL
	((CPortal_Player *)pPlayer)->DoAnimationEvent(PLAYERANIMEVENT_ATTACK_PRIMARY, 0);
#endif

	FireBulletsInfo_t info;
	info.m_vecSrc	 = pPlayer->Weapon_ShootPosition( );
	
	info.m_vecDirShooting = pPlayer->GetAutoaimVector( AUTOAIM_SCALE_DEFAULT );

	// To make the firing framerate independent, we may have to fire more than one bullet here on low-framerate systems, 
	// especially if the weapon we're firing has a really fast rate of fire.
	info.m_iShots = 0;
	float fireRate = GetFireRate();

	while ( m_flNextPrimaryAttack <= gpGlobals->curtime )
	{
		// MUST call sound before removing a round from the clip of a CMachineGun
		WeaponSound(SINGLE, m_flNextPrimaryAttack);
		m_flNextPrimaryAttack = m_flNextPrimaryAttack + fireRate;
		info.m_iShots++;
		if ( !fireRate )
			break;
	}

	info.m_iShots = MIN(info.m_iShots, m_iClip1);
	m_iClip1 -= info.m_iShots;

	info.m_flDistance = MAX_TRACE_LENGTH;
	info.m_iAmmoType = m_iPrimaryAmmoType;
	info.m_iTracerFreq = 2;

#if !defined( CLIENT_DLL )
	// Fire the bullets
	info.m_vecSpread = pPlayer->GetAttackSpread( this );
#else
	//!!!HACKHACK - what does the client want this function for? 
	info.m_vecSpread = GetActiveWeapon()->GetBulletSpread();
#endif // CLIENT_DLL

	pPlayer->FireBullets( info );

	if (!m_iClip1 && pPlayer->GetAmmoCount(m_iPrimaryAmmoType) <= 0)
	{
		// HEV suit - indicate out of ammo condition
		pPlayer->SetSuitUpdate("!HEV_AMO0", FALSE, 0); 
	}

	//Add our view kick in
	AddViewKick();
}


int CWeaponPistol::GetWorldModelIndex( void )
{
	if ( !m_bCurrentlyDualPistols || m_inPrecache )
	{
		return m_iWorldModelIndex;
	}
	else
	{
		return m_silencedModelIndex;
	}
}


const char * CWeaponPistol::GetWorldModel(void) const
{
	if ( !m_bCurrentlyDualPistols || m_inPrecache )
	{
		return BaseClass::GetWorldModel();
	}
	else
	{
		return GetWpnData().szWorldModelAkimbo;
	}
}
