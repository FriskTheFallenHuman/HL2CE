//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#include "cbase.h"
#include "weapon_citizenpackage.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

IMPLEMENT_SERVERCLASS_ST(CWeaponCitizenPackage, DT_WeaponCitizenPackage)
END_SEND_TABLE()

BEGIN_DATADESC( CWeaponCitizenPackage )
END_DATADESC()

LINK_ENTITY_TO_CLASS( weapon_citizenpackage, CWeaponCitizenPackage );
PRECACHE_WEAPON_REGISTER(weapon_citizenpackage);

acttable_t	CWeaponCitizenPackage::m_acttable[] = 
{
	{ ACT_IDLE,						ACT_IDLE_PACKAGE,					false },
	{ ACT_WALK,						ACT_WALK_PACKAGE,					false },

	{ ACT_MP_STAND_IDLE,				ACT_DOD_STAND_IDLE_TNT,				false },
	{ ACT_MP_CROUCH_IDLE,				ACT_DOD_CROUCH_IDLE_TNT,				false },
	{ ACT_MP_RUN,						ACT_DOD_WALK_IDLE_TNT,					false },
	{ ACT_MP_CROUCHWALK,				ACT_DOD_CROUCHWALK_IDLE_TNT,			false },
	{ ACT_MP_ATTACK_STAND_PRIMARYFIRE,	ACT_DOD_PRIMARYATTACK_GREN_FRAG,			false },
	{ ACT_MP_ATTACK_CROUCH_PRIMARYFIRE,	ACT_DOD_PRIMARYATTACK_GREN_FRAG,			false },
	//{ ACT_MP_RELOAD_STAND,				ACT_DOD_RELOAD_MP40,					false },
	//{ ACT_MP_RELOAD_CROUCH,				ACT_DOD_RELOAD_CROUCH_MP40,				false },
	{ ACT_MP_JUMP,						ACT_MP_JUMP,							false },

};
IMPLEMENT_ACTTABLE(CWeaponCitizenPackage);

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CWeaponCitizenPackage::ItemPostFrame( void )
{
	// Do nothing
}

//-----------------------------------------------------------------------------
// Purpose: Remove the citizen package if it's ever dropped
//-----------------------------------------------------------------------------
void CWeaponCitizenPackage::Drop( const Vector &vecVelocity )
{
	BaseClass::Drop( vecVelocity );
	UTIL_Remove( this );
}



//-----------------------------------------------------------------------------
// Purpose: Citizen suitcase
//-----------------------------------------------------------------------------
class CWeaponCitizenSuitcase : public CWeaponCitizenPackage
{
	DECLARE_CLASS( CWeaponCitizenSuitcase, CWeaponCitizenPackage );
public:
	DECLARE_SERVERCLASS();
	DECLARE_DATADESC();	
	DECLARE_ACTTABLE();
};

IMPLEMENT_SERVERCLASS_ST(CWeaponCitizenSuitcase, DT_WeaponCitizenSuitcase)
END_SEND_TABLE()

BEGIN_DATADESC( CWeaponCitizenSuitcase )
END_DATADESC()

LINK_ENTITY_TO_CLASS( weapon_citizensuitcase, CWeaponCitizenSuitcase );
PRECACHE_WEAPON_REGISTER(weapon_citizensuitcase);

acttable_t	CWeaponCitizenSuitcase::m_acttable[] = 
{
	{ ACT_IDLE,						ACT_IDLE_SUITCASE,					false },
	{ ACT_WALK,						ACT_WALK_SUITCASE,					false },

	{ ACT_MP_STAND_IDLE,				ACT_MP_STAND_IDLE,					false },
	{ ACT_MP_CROUCH_IDLE,				ACT_MP_CROUCH_IDLE,					false },
	{ ACT_MP_RUN,						ACT_MP_RUN,							false },
	{ ACT_MP_CROUCHWALK,				ACT_MP_CROUCHWALK,					false },
	{ ACT_MP_ATTACK_STAND_PRIMARYFIRE,	ACT_MP_ATTACK_STAND_PRIMARYFIRE,	false },
	{ ACT_MP_ATTACK_CROUCH_PRIMARYFIRE,	ACT_MP_ATTACK_CROUCH_PRIMARYFIRE,	false },
	//{ ACT_MP_RELOAD_STAND,			ACT_DOD_RELOAD_MP40,				false },
	//{ ACT_MP_RELOAD_CROUCH,			ACT_DOD_RELOAD_CROUCH_MP40,			false },
	{ ACT_MP_JUMP,						ACT_MP_JUMP,						false },
};
IMPLEMENT_ACTTABLE(CWeaponCitizenSuitcase);
